This extension is for users that want their homepages as pinned tabs when they start Firefox.

Firefox will normally remember and restore tabs that are pinned.
However, if you have Firefox set to clear the browsing history when Firefox closes, all the tabs (pinned or not) are forgotten.

This extension will change the homepage tabs that are opened to be pinned tabs.
The tabs are changed in order as they are loaded so the pinned tabs remain in the same order.

Any tabs that start with "about:" are skipped.
This allows a homepage entry of "about:blank" or "about:newtab" to be set and left as a regular tab.

The extension also includes a navbar button that will pin all existing tabs (except for tabs starting with "about:").

This extension was inspired by the App Tab Initializer plugin.
https://addons.mozilla.org/en-US/firefox/addon/app-tab-initializer/
