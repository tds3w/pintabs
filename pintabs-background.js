function getCurrentWindowTabs() {
	return browser.tabs.query({currentWindow: true});
}

function pinTabs() {
	getCurrentWindowTabs().then((tabs) => {
		for (var tab of tabs) {
			console.log(tab.url);
			if (tab.url.startsWith('about:')) {
				browser.tabs.update(tab.id, {pinned: false});
			} else {
				browser.tabs.update(tab.id, {pinned: true});
			}
		}
	});
}

function pinTab(tabId) {
	browser.tabs.get(tabId).then((tab) => {
		console.log('TabUrl: ' + tab.url);
		if (tab.url.startsWith('about:')) {
			browser.tabs.update(tab.id, {pinned: false});
		} else {
			browser.tabs.update(tab.id, {pinned: true});
		}
	});
}

function waitForTabs() {
	console.log('WaitCount: ' + waitCount);
	getCurrentWindowTabs().then((tabs) => {
		for (var tab of tabs) {
			console.log('TabId: ' + tab.id);
			console.log('TabStatus: ' + tab.status);
			if (tab.status == "complete") {
				pinTab(tab.id);
			} else {
				if (waitCount < 20) {
					waitCount++;
					setTimeout(waitForTabs, 500);
				}

				break;
			}
		}
	});
}

var waitCount = 0;

setTimeout(waitForTabs, 1000);

browser.browserAction.onClicked.addListener(pinTabs);
