function getCurrentWindowTabs() {
	return browser.tabs.query({currentWindow: true});
}

document.addEventListener("click", (e) => {
	if (e.target.id === "pin-tabs") {
		getCurrentWindowTabs().then((tabs) => {
			for (var tab of tabs) {
				console.log(tab.url);
				if (tab.url.startsWith('about:')) {
					browser.tabs.update(tab.id, {pinned: false});
				} else {
					browser.tabs.update(tab.id, {pinned: true});
				}
			}
		});
	}

	e.preventDefault();
});
